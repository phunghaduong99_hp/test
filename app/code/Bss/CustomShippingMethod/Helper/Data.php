<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category  BSS
 * @package   Bss_AdminShippingMethod
 * @author    Extension Team
 * @copyright Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license   http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CustomShippingMethod\Helper;

use Bss\CustomShippingMethod\Model\ResourceModel\CustomMethod\CollectionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\OfflineShipping\Model\Carrier\Flatrate\ItemPriceCalculator;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\State;
use Bss\CustomShippingMethod\Model\ResourceModel\StoreView;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 *
 * @package Bss\CustomShippingMethod\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var State
     */
    protected $state;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var StoreView
     */
    protected $storeView;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var ItemPriceCalculator
     */
    private $itemPriceCalculator;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param StoreManagerInterface $store
     * @param ItemPriceCalculator $itemPriceCalculator
     * @param CollectionFactory $collectionFactory
     * @param State $state
     * @param StoreView $storeView
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $store,
        ItemPriceCalculator $itemPriceCalculator,
        CollectionFactory $collectionFactory,
        State $state,
        StoreView $storeView
    ) {
        parent::__construct($context);
        $this->state = $state;
        $this->storeManager = $store;
        $this->storeView = $storeView;
        $this->collectionFactory = $collectionFactory;
        $this->itemPriceCalculator = $itemPriceCalculator;
    }

    /**
     * Get value Active.
     *
     * @param int $storeId
     * @return mixed
     */
    public function getActive($storeId)
    {
        return $this->scopeConfig->getValue(
            'carriers/customshippingmethod/active',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get Store Manager.
     *
     * @return StoreManagerInterface
     */
    public function getStoreMana(): StoreManagerInterface
    {
        return $this->storeManager;
    }

    /**
     * Get State.
     *
     * @return State
     */
    public function getState(): State
    {
        return $this->state;
    }

    /**
     * Get Store View.
     *
     * @return StoreView
     */
    public function getStoreView(): StoreView
    {
        return $this->storeView;
    }

    /**
     * Get Collection Method.
     *
     * @return CollectionFactory
     */
    public function getCollectionMethod(): CollectionFactory
    {
        return $this->collectionFactory;
    }

    /**
     * ItemPriceCalculator
     *
     * @return ItemPriceCalculator
     */
    public function itemPriceCalculator(): ItemPriceCalculator
    {
        return $this->itemPriceCalculator;
    }
}
