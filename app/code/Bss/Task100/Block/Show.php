<?php


namespace Bss\Task100\Block;

use Bss\Task100\Model\ResourceModel\Internship\Collection;
use Magento\Framework\App\Request\Http;
use Magento\Framework\View\Element\Template;

/**
 *Class Display
 * @package Bss\Task100\Block;
 */
class Show extends Template
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var Http
     */
    private $request;

    public function __construct(Template\Context $context,
                                Collection $collection,
                                Http $request)
    {
        $this->request = $request;
        $this->collection = $collection;
        parent::__construct($context);
    }

    /**
     * @return array|mixed|null
     */
    public function getItemById(): ?array
    {
        $id = $this->request->getParam('id');
        if($this->collection->getItemById($id) != null)
            return $this->collection->getItemById($id)->getData();
        else return [];
    }
}
