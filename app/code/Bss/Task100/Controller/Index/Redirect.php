<?php


namespace  Bss\Task100\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;


/**
 * Class Redirect
 * @package Bss\Task100\Controller\Index
 */
class Redirect extends Action
{
    /**
     * @var RedirectFactory
     */
    protected $redirectFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Redirect constructor.
     * @param RedirectFactory $redirectFactory
     * @param StoreManagerInterface $storeManager
     * @param Context $context
     */
    public function __construct(
        RedirectFactory $redirectFactory,
        StoreManagerInterface $storeManager,
        Context $context
    ) {
        $this->_storeManager = $storeManager;
        $this->redirectFactory = $redirectFactory;
        return parent::__construct($context);
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Redirect|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {

        return $this->redirectFactory->create()
            ->setPath($this->_storeManager->getStore()->getBaseUrl().'cms/index/index');
    }
}
