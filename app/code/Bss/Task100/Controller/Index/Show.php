<?php
namespace Bss\Task100\Controller\Index;

use Bss\Task100\Model\Internship;
use Laminas\Db\Sql\Ddl\Column\Datetime;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;

class Show extends Action
{

    protected $internshipModel;

    protected $request;

    protected $_pageFactory;

    protected $internshipResource;


    /**
     * Add constructor.
     * @param Context $context
     * @param Internship $internshipModel
     * @param \Bss\Task100\Model\ResourceModel\Internship $internshipResource
     * @param PageFactory $pageFactory
     * @param Http $request
     */
    public function __construct(
        Context $context,
        Internship $internshipModel,
        \Bss\Task100\Model\ResourceModel\Internship $internshipResource,
        PageFactory $pageFactory,
        Http $request
    ) {
        $this->request = $request;
        $this->_pageFactory = $pageFactory;
        $this->internshipModel = $internshipModel;
        $this->internshipResource = $internshipResource;
        return parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|\Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function execute()
    {

        return $this->_pageFactory->create();

    }
}
