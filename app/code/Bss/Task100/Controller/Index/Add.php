<?php
namespace Bss\Task100\Controller\Index;

use Bss\Task100\Model\Internship;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;

class Add extends Action implements HttpPostActionInterface
{

    /**
     * @var Internship
     */
    protected $internshipModel;

    /**
     * @var \Bss\Task100\Model\ResourceModel\Internship
     */
    protected $internshipResource;


    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Add constructor.
     * @param Context $context
     * @param Internship $internshipModel
     * @param \Bss\Task100\Model\ResourceModel\Internship $internshipResource
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        Internship $internshipModel,
        \Bss\Task100\Model\ResourceModel\Internship $internshipResource,
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
        $this->internshipModel = $internshipModel;
        $this->internshipResource = $internshipResource;
        return parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|\Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $posts = $this->getRequest()->getParams();
        if( isset($posts['name']) && isset($posts['description']) && isset($posts['avatar']) ){
            $posts['dob'] = '2021-01-22 15:40';
            $model = $this->internshipModel->setData($posts);
            $this->internshipResource->save($model);
        }
        return $this->_redirect($this->storeManager->getStore()->getBaseUrl().'task100/index/index',
            ['status' => 'success']
        );
    }
}
