<?php


namespace  Bss\Task100\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\Result\ForwardFactory;


/**
 * Class Redirect
 * @package Bss\Task100\Controller\Index
 */
class Forward extends Action implements HttpGetActionInterface
{
    /**
     * @var ForwardFactory
     */
    protected $forwardFactory;

    /**
     * Redirect constructor.
     * @param ForwardFactory $forwardFactory
     * @param Context $context
     */
    public function __construct(
        ForwardFactory $forwardFactory,
        Context $context
    ) {
        $this->forwardFactory = $forwardFactory;
        return parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $result = $this->forwardFactory->create();
        $result->setController('index');
        $result->setModule('cms');
        $result->forward('index');
        return $result;
    }
}
