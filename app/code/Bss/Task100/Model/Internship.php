<?php
namespace Bss\Task100\Model;

use Magento\Framework\Model\AbstractModel;

class Internship extends  AbstractModel{

    protected function _construct()
    {
        $this->_init(\Bss\Task100\Model\ResourceModel\Internship::class);
    }
}

