<?php


namespace Bss\Task100\Model\ResourceModel\Internship;



use Bss\Task100\Model\Internship;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    public function _construct()
    {
        $this->_init(Internship::class, \Bss\Task100\Model\ResourceModel\Internship::class);
    }
}
