<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Bss\Task100\Setup\Patch\Schema;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

/**
 */
class AddData
    implements DataPatchInterface,
    PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * AddData constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * @return AddData|void
     */
    public function apply(): AddData
    {

        $this->moduleDataSetup->getConnection()->startSetup();
        $data = [
            ['name' => 'Phung Ha Duong', 'avatar' => 'https://pms.bssgroup.vn/default/viewtaskdetail/InternIL-98', 'description' => 'OKEEEEEEE1', 'dob' => '2021-01-22 15:40' ],
            ['name' => 'Dinh Thuy Ha', 'avatar' => 'https://pms.bssgroup.vn/default/viewtaskdetail/InternIL-99', 'description' => 'OKEEEEEEE2', 'dob' => '2021-01-22 15:40' ],
            ['name' => 'Nguyen Trong Huan', 'avatar' => 'https://pms.bssgroup.vn/default/viewtaskdetail/InternIL-100', 'description' => 'OKEEEEEEE3', 'dob' => '2021-01-22 15:40' ],
            ['name' => 'Hung', 'avatar' => 'https://pms.bssgroup.vn/default/viewtaskdetail/InternIL-101', 'description' => 'OKEEEEEEE4', 'dob' => '2021-01-22 15:40' ],
            ['name' => 'Thuy Dung', 'avatar' => 'https://pms.bssgroup.vn/default/viewtaskdetail/InternIL-102', 'description' => 'OKEEEEEEE5', 'dob' => '2021-01-22 15:40' ],

        ];
        $this->moduleDataSetup->getConnection()->insertArray(
            $this->moduleDataSetup->getTable('internship_table'),
            ['name', 'avatar', 'description', 'dob'],
            $data
        );
        $this->moduleDataSetup->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies(): array
    {
        return [
        ];
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases(): array
    {

        return [];
    }
}
